#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "DYAlertController.h"

FOUNDATION_EXPORT double DCAlertControllerVersionNumber;
FOUNDATION_EXPORT const unsigned char DCAlertControllerVersionString[];

