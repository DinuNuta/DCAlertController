# DCAlertController

[![CI Status](https://img.shields.io/travis/DinuNuta/DCAlertController.svg?style=flat)](https://travis-ci.org/DinuNuta/DCAlertController)
[![Version](https://img.shields.io/cocoapods/v/DCAlertController.svg?style=flat)](https://cocoapods.org/pods/DCAlertController)
[![License](https://img.shields.io/cocoapods/l/DCAlertController.svg?style=flat)](https://cocoapods.org/pods/DCAlertController)
[![Platform](https://img.shields.io/cocoapods/p/DCAlertController.svg?style=flat)](https://cocoapods.org/pods/DCAlertController)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

DCAlertController is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'DCAlertController'
```

## Author

DinuNuta, coscodan.dinu@gmail.com

## License

DCAlertController is available under the MIT license. See the LICENSE file for more info.
